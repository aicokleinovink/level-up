import environment from './environment';
import {PLATFORM} from 'aurelia-pal';
import {HttpClient} from 'aurelia-fetch-client';
import 'babel-polyfill';
import * as Bluebird from 'bluebird';

// remove out if you don't want a Promise polyfill (remove also from webpack.config.js)
Bluebird.config({warnings: {wForgottenReturn: false}});

export function configure(aurelia) {
  // set base url for expressJS backend to fetch todos
  const httpClient = new HttpClient().configure(aConfig => {
    aConfig.withBaseUrl('http://localhost:4000');
    aConfig.rejectErrorResponses();
  });

  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'));

  // Uncomment the line below to enable animation.
  // aurelia.use.plugin(PLATFORM.moduleName('aurelia-animator-css'));
  // if the css animator is enabled, add swap-order="after" to all router-view elements

  // Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  // aurelia.use.plugin(PLATFORM.moduleName('aurelia-html-import-template-loader'));

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  // register our configured httpClient
  aurelia.container.registerInstance(HttpClient, httpClient);

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
