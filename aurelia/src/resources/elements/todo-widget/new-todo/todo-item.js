export class TodoItem {
  text;
  done;
  dateAdded;

  constructor(text = '', done = false, dateAdded = new Date().toISOString()) {
    this.text = text;
    this.done = done;
    this.dateAdded = dateAdded;
  }
}
