import {bindable, computedFrom} from 'aurelia-framework';
import {TodoItem} from "./todo-item";

export class NewTodo {
  @bindable() onSubmit;
  todoItem;

  @computedFrom('todoItem.text')
  get todoItemIsValid() {
    return this.todoItem.text.length > 0;
  }

  constructor() {
    this.todoItem = new TodoItem();
  }

  submitTodo() {
    this.todoItem.dateAdded = new Date().toISOString();
    this.onSubmit(this.todoItem);
    this.todoItem = new TodoItem();
  }
}
