import {bindable, inject, BindingEngine} from 'aurelia-framework';

@inject(BindingEngine)
export class Todo {
  @bindable() todo;
  @bindable() onChange;

  bindingEngine;
  observerSubscription;

  constructor(bindingEngine) {
    this.todo = null;
    this.bindingEngine = bindingEngine;
    this.observerSubscription = null;
  }

  attached() {
    this.observerSubscription = this.bindingEngine
      .propertyObserver(this.todo, 'done')
      .subscribe((newValue, oldValue) => this.todoChanged(newValue, oldValue));
  }

  detached() {
    this.observerSubscription.dispose();
  }

  todoChanged(newValue, oldValue) {
    if (oldValue === null) {
      return;
    }

    this.onChange(this.todo);
  }
}
