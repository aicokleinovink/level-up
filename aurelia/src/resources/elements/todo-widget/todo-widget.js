import {inject, bindable} from 'aurelia-framework';
import {TodoService} from "../../../services/todo/todo.service";

@inject(TodoService)
export class TodoWidget {
  @bindable() todos;
  todoService;

  constructor(todoService) {
    this.todos = [];
    this.todoService = todoService;
  }

  addTodo(todo) {
    this.todoService.postTodo(todo)
      .then(newTodo => this.todos = [newTodo, ...this.todos])
      .catch(console.error);
  }

  updateTodo(todo) {
    this.todoService.updateTodo(todo)
      .then(() => console.info('todo updated'))
      .catch(console.error);
  }
}
