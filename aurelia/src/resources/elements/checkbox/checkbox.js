import {bindable} from 'aurelia-framework';

export class Checkbox {
  @bindable() checked;

  constructor() {
    this.checked = false;
  }
}
