import {inject} from 'aurelia-framework'
import {HttpClient, json} from 'aurelia-fetch-client'

@inject(HttpClient)
export class TodoService {
  httpClient;

  constructor(httpClient) {
    this.httpClient = httpClient;
  }

  getTodos() {
    return this.httpClient.fetch('/todos')
      .then(response => response.json())
  }

  postTodo(todo) {
    return this.httpClient.fetch(`/todos`, {
      method: 'POST',
      body: json(todo)
    }).then(response => response.json());
  }

  updateTodo(todo) {
    return this.httpClient.fetch(`/todos/${todo.id}`, {
      method: 'PUT',
      body: json(todo)
    }).then(response => response.json());
  }
}
