import {inject} from 'aurelia-framework';
import {TodoService} from './services/todo/todo.service';

@inject(TodoService)
export class App {
  todoService;
  todos;
  fetchError;

  constructor(todoService) {
    this.todoService = todoService;
    this.todos = [];
    this.fetchError = false;
  }

  attached() {
    this.todoService.getTodos()
      .then(todos => this.todos = todos)
      .catch(error => {
        this.fetchError = true;
        console.error(error);
      });
  }
}
