import * as React from "react";
import * as ReactDOM from "react-dom";

interface TodoFormState { text: string; }
interface TodoFormProps { addTodo: (value: ToDoItem) => void; }

export class TodoForm extends React.Component<TodoFormProps, TodoFormState> {
  constructor(props: any) {
    super(props);
    this.state = {text: ''};
  }

  handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    this.setState({
      text: event.currentTarget.value
    });
  }

  handleSubmit = (event: React.FormEvent<any>) => {
    this.props.addTodo({
      text: this.state.text,
      done: false,
      dateAdded: new Date()
    });
    this.setState({
      text: ''
    });
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group">
          <input className="form-control col-md-12" onChange={this.handleChange} value={this.state.text} />
        </div>
        <input type="submit" value="Add" className="btn btn-primary" />
      </form>
    );
  }
}
