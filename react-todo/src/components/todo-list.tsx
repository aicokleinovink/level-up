import * as React from "react";
import * as ReactDOM from "react-dom";

import { Todo } from './todo-item'

interface TodoListProps {
  todos: ToDoItem[];
  remove: (todo: ToDoItem) => void;
  done: (todo: ToDoItem) => void;
}

export class TodoList extends React.Component<TodoListProps, {}> {

  render() {
    const todoElements = this.props.todos.map((todo, index) => {
      return (<Todo todo={todo} remove={this.props.remove} done={this.props.done} 
        key={index}/>)
    });
    return (<div className="list-group" style={{marginTop:'30px'}}>{todoElements}</div>);
  }
}
