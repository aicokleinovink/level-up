export class TodoItem {
  public text: string;
  public done: boolean;
  public dateAdded: string;

  constructor(text: string = '', done: boolean = false, dateAdded: string = new Date().toISOString()) {
    this.text = text;
    this.done = done;
    this.dateAdded = dateAdded;
  }
}
