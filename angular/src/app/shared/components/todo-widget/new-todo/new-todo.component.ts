import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {TodoItem} from './todo-item.class';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'new-todo',
  templateUrl: 'new-todo.component.html'
})
export class NewTodoComponent implements OnInit {
  @Output() onSubmit: EventEmitter<TodoItem>;
  public todoForm: FormGroup;

  constructor() {
    this.onSubmit = new EventEmitter<TodoItem>();
  }

  ngOnInit(): void {
    this.todoForm = new FormGroup({
      todo: new FormControl('', Validators.required)
    });
  }

  submitTodo(): void {
    const todoItem = new TodoItem(this.todoForm.controls.todo.value);
    this.onSubmit.emit(todoItem);
    this.todoForm.reset();
  }
}
