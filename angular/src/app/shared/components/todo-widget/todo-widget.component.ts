import {Component, Input} from '@angular/core';
import {TodoService} from '../../services/todo/todo.service';
import {Todo} from '../../services/todo/todo.interface';

@Component({
  selector: 'todo-widget',
  templateUrl: './todo-widget.html',
  styleUrls: ['./todo-widget.scss']
})
export class TodoWidgetComponent {
  @Input() todos: Todo[];

  constructor(private todoService: TodoService) {
    this.todos = [];
  }

  addTodo(todo: Todo): void {
    this.todoService.postTodo(todo);
  }

  updateTodo(todo: Todo): void {
    this.todoService.updateTodo(todo);
  }
}
