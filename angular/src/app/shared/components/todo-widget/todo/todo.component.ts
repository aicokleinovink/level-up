import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Todo} from '../../../services/todo/todo.interface';

@Component({
  selector: 'todo',
  templateUrl: 'todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent {
  @Input() todo: Todo;
  @Output() onChange: EventEmitter<Todo>;

  constructor() {
    this.todo = null;
    this.onChange = new EventEmitter<Todo>();
  }

  public todoChecked(isChecked: boolean): void {
    this.todo.done = isChecked;
    this.onChange.emit(this.todo);
  }
}
