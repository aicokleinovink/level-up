import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'checkbox',
  templateUrl: 'checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {
  @Input() checked: boolean;
  @Output() onChange: EventEmitter<boolean>;

  constructor() {
    this.checked = false;
    this.onChange = new EventEmitter<boolean>();
  }

  checkIt(): void {
    this.checked = !this.checked;
    this.onChange.emit(this.checked);
  }
}
