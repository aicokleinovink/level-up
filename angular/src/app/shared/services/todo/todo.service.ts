import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Observable} from 'rxjs/Observable';
import {Todo} from './todo.interface';

const baseUrl = 'http://localhost:4000';

@Injectable()
export class TodoService {
  private _todos: BehaviorSubject<Todo[]>;
  private dataStore: {
    todos: Todo[]
  };

  public get todos(): Observable<Todo[]> {
    return this._todos.asObservable();
  }

  constructor(private httpClient: HttpClient) {
    this._todos = new BehaviorSubject([]);
    this.dataStore = {
      todos: []
    };
  }

  public loadTodos(): void {
    this.httpClient.get(`${baseUrl}/todos`).subscribe((todos: Todo[]) => {
      this.dataStore.todos.push(...todos);
      this._todos.next(Object.assign({}, this.dataStore).todos);
    }, console.error);
  }

  public postTodo(todo: Todo): void {
    this.httpClient.post(`${baseUrl}/todos`, todo).subscribe((newTodo: Todo) => {
      this.dataStore.todos = [newTodo, ...this.dataStore.todos];
      this._todos.next(Object.assign({}, this.dataStore).todos);
    }, console.error);
  }

  public updateTodo(todo: Todo): void {
    this.httpClient.put(`${baseUrl}/todos/${todo.id}`, todo).subscribe((updatedTodo: Todo) => {
      const dataStoreIndex = this.getDataStoreIndex(updatedTodo);
      if (dataStoreIndex >= 0) {
        this.dataStore.todos[dataStoreIndex] = updatedTodo;
      } else {
        this.dataStore.todos.push(updatedTodo);
      }
      this._todos.next(Object.assign({}, this.dataStore).todos);
    });
  }

  private getDataStoreIndex(todo: Todo): number {
    let dataStoreIndex = -1;
    this.dataStore.todos.forEach((storedTodo, index) => {
      if (storedTodo.id === todo.id) {
        dataStoreIndex = index;
      }
    });
    return dataStoreIndex;
  }
}
