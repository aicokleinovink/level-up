import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {TodoWidgetComponent} from './shared/components/todo-widget/todo-widget.component';
import {TodoService} from './shared/services/todo/todo.service';
import {HttpClientModule} from '@angular/common/http';
import {TodoComponent} from './shared/components/todo-widget/todo/todo.component';
import {NewTodoComponent} from './shared/components/todo-widget/new-todo/new-todo.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CheckboxComponent} from './shared/components/checkbox/checkbox.component';


@NgModule({
  declarations: [
    AppComponent,
    TodoWidgetComponent,
    TodoComponent,
    NewTodoComponent,
    CheckboxComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    TodoService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
