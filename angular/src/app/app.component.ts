import {Component, OnInit} from '@angular/core';
import {TodoService} from './shared/services/todo/todo.service';
import {Observable} from 'rxjs/Observable';
import {Todo} from './shared/services/todo/todo.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  todos: Observable<Todo[]>;

  constructor(private todoService: TodoService) {
    this.todos = this.todoService.todos;
  }

  ngOnInit() {
    this.todoService.loadTodos();
  }
}
